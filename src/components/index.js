/* eslint-disable linebreak-style */
import Header from './Header.vue';
import OrderState from './OrderState.vue';
import Product from './Product.vue';
import PaymentOptions from './PaymentOptions.vue';
import PaymentList from './PaymentList.vue';
import Footer from './Footer.vue';

export {
  Header, OrderState, Product, PaymentOptions, PaymentList, Footer,
};
