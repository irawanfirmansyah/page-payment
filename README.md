# page-payment
This mini-task is a UI Application Project that I create independetly, with JavaScript Framework ([Vue](https://vuejs.org/)), pure CSS (with [BEM](http://getbem.com/introduction/) methodology), and hard-coded.

To try and demonstrate it, it's pretty easy to do that in your local as the steps follow below :
1) Pull / Clone the repository into your local directory.
2) Open the directory on your favorite text editor.
3) Install the environment and basic denpendecies needed.
   
    ```
    npm install
    ```
4) Once the installation finished, run the app in your local.

    ```
    npm run serve
    ```
5) Open the application on your browser, the app regularly run in `localhost:8080`.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
